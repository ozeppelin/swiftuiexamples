//
//  LandmarkDetail.swift
//  Landmarks
//
//  Created by Anderson Silva on 04/07/19.
//  Copyright © 2019 Anderson Silva. All rights reserved.
//

import SwiftUI

struct LandmarkDetail : View {
    
    @EnvironmentObject var userData: UserData
    var landmark: Landmark
    
    var landmarkIndex: Int {
        userData.landmarks.firstIndex(where: { $0.id == landmark.id })!
    }
    
    var body: some View {
        ScrollView {
            VStack {
                MapView(coordinate: landmark.locationCoordinate)
                    .edgesIgnoringSafeArea(.top)
                    .frame(width: nil, height: Length(300), alignment: .top)
                
                CircleImage(image: landmark.image(forSize: 250))
                    .offset(y: -130)
                    .padding(.bottom, Length(-130))
                
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(landmark.name)
                            .font(.title)
                        
                        Button(action: {
                            self.userData.landmarks[self.landmarkIndex].isFavorite.toggle()
                        }) {
                            if self.userData.landmarks[self.landmarkIndex].isFavorite {
                                Image(systemName: "star.fill")
                                    .foregroundColor(.yellow)
                            }else{
                                Image(systemName: "star")
                                    .foregroundColor(.gray)
                            }
                        }
                        
                    }
                    
                    HStack(alignment: .top) {
                        Text(landmark.park)
                            .font(.subheadline)
                        Spacer()
                        Text(landmark.state)
                            .font(.subheadline)
                    }
                    
                }
                .padding()
                Spacer()
            }
            .navigationBarTitle(Text(landmark.name), displayMode: .automatic)
        }
        .edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct LandmarkDetail_Previews : PreviewProvider {
    static var previews: some View {
        LandmarkDetail(landmark: landmarkData[0])
    }
}
#endif
