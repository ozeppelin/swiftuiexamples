//
//  ContentView.swift
//  Landmarks
//
//  Created by Anderson Silva on 04/07/19.
//  Copyright © 2019 Anderson Silva. All rights reserved.
//

//import SwiftUI
//
//struct ContentView : View {
//    var body: some View {
//        VStack {
//            MapView()
//                .edgesIgnoringSafeArea(.top)
//                .frame(width: nil, height: Length(300), alignment: .top)
//            
//            CircleImage()
//                .offset(y: -130)
//                .padding(.bottom, Length(-130))
//            
//            
//            VStack(alignment: .leading) {
//                Text("Turtle Rock")
//                    .font(.title)
//                HStack(alignment: .top) {
//                    Text("Joshua Tree National Park").font(.subheadline)
//                    Spacer()
//                    Text("California").font(.subheadline)
//                }
//                
//            }
//            .padding()
//            Spacer()
//        }
//    }
//}
//
//#if DEBUG
//struct ContentView_Previews : PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
//#endif
